from app import app, network
from flask import render_template, request

@app.route('/admin/')
@app.route('/admin/dashboard')
def dashboard():
    return render_template('private/dashboard.html', title='Admin')

@app.route('/admin/devices')
def devices():
    return render_template('private/devices.html', title='Devices', devices=network.device_cache)

@app.route('/admin/devices/scan')
def device_scan():
    data = [[x.name,x.vendor,x.mac,x.ip] for x in network.scan()]
    return render_template('private/devices_scan.html', devices=data)