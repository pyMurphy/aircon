import socket
import nmap

nm = nmap.PortScanner()
device_cache=[]

class Device:
    def __init__(self,data):
        self.name=data['hostnames'][0].get('name','Unknown') or 'Unknown'
        self.ip=data['addresses'].get('ipv4','Unknown')
        self.mac=data['addresses'].get('mac','Unknown')
        self.vendor=data['vendor'].get(self.mac,'Unknown')

def gethost():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    return ip

def scan():
    global device_cache
    nm.scan(hosts='192.168.0.0/24', arguments='-sP')
    results=[Device(nm[x]) for x in nm.all_hosts()]
    if len(results) != 0:
        device_cache=[[x.name,x.vendor,x.mac,x.ip] for x in results]
    return results
