class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'this-is-a-secret'

class ProductionConfig(Config):
    DB_NAME = 'production-db'

class DevelopmentConfig(Config):
    DB_NAME = 'development-db'
    DEBUG = True